'use strict'

const express = require('express');
const router = express.Router();
const User = require('./../models/user');

// index route
router.get('/users', function(req, res) {
  User.find(function(err, users) {
    res.json(users)
  })
})

// show/view record
router.get('/users/:id', function(req, res) {
  User.findOne(req.params.username, function(err, user) {
    if (err) {
      res.status(500).send('ERROR! User not found.')
    } else {
      res.json(user)
    }
  })
})

// create a new record
router.post('/users', function(req, res) {
  const user = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    username: req.body.username,
    password: req.body.password,
  })

  user.save(function(err, user) {
    if (err) {
      res.json(err)
    } else {
      res.status(201)
      res.json(user)
    }
  })
})

// update/modify goes to put/patch
router.put('/users/:id', function(req, res) {
  User.findByIdAndUpdate(req.params.id, {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    username: req.body.username,
    password: req.body.password,
    dateUpdated: new Date(),
  }, { new: true, runValidators: true }, function (err, user) {
    if (err) {
      res.json(err)
    } else {
      res.json(user)
    }
  })
})

// destroy/delete
router.delete('/users/:id', function(req, res) {
  User.findByIdAndUpdate(req.params.id, (err, user) => {
    dateDeleted: new Date()
  }, { new: true }, function (err, user) {
    if (err) {
      res.json(err)
    } else {
      res.json(user)
    }
  })
})

// search by name
router.get('/users/search/:search', function(req, res) {
  User.findByUsername(req.params.search, function(err, users) {
    res.json(users)
    console.log(req.query)
  })
})

// make the object available to other
module.exports = router
