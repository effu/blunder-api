'use strict'

const express = require('express')
const router = express.Router()
// const mongoose = require('mongoose')
// const Item = require('./../models/item.js')
const Group = require('./../models/group.js')

// index route
router.get('/groups', function(req, res) {
  Group.find(function(err, groups) {
    if (err) {
      res.json(err)
    } else {
      res.json(groups)
    }
  })
//  res.json(Groups.find())
})

// show/view record
router.get('/groups/:id', function(req, res) {

  Group.findById(req.params.id, function(err, group) {
    if (err) {
      res.status(500).send('ERROR! Group not found.')
    } else {
      res.json(group)
    }

      // res.json({msg: 'Group not found!'};
  })
})

// create a new record
router.post('/groups', function(req, res) {
  const group = new Group(req.body)

  group.save(function(err) {
    if (err) {
      res.json(err)
    } else {
      res.status(201)
      res.json('Group created successfully!')
    }
  })
  // res.json({
  //   'Groups post create': req.params
  // })
})

// update/modify goes to put/patch
// look up in the database
// update the models properties with their database
// save changes into database

router.put('/groups/:id', function(req, res) {
  Group.findByIdAndUpdate(req.params.id, {
    name: req.body.name
  }, {new: true}, function (err, group) {
    if (err) {
      res.json(err)
    } else {
      res.json(group)
    }
  })
})
// router.put('/groups/:id', function(req, res) {
//   Group.findById(req.params.id, function(err, group) {
//     if (err) {
//       res.status(500).send(err)
//     } else {
//       // assign name value to what they sent
//       group.name = req.body.name || group.name;
//       group.save(function (err, group) {
//         if (err) {
//           res.status(500).send(err)
//         }
//         res.send(group)
//       })
//     }
//   })
// })
//
// destroy/delete
router.delete('/groups/:id', function(req, res) {
  Group.findById(req.params.id, function(err, group) {
    if (err) {
      res.status(500).send(err)
    } else {
      // assign name value to what they sent
      group.remove(function (err) {
        if (err) {
          res.status(500).send(err)
        }
        res.json('Success! Group deleted.')
      })
    }
  })
})


// make the object available to other
module.exports = router
