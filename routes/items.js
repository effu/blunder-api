'use strict'

const express = require('express');
const router = express.Router();
// const mongoose = require('mongoose');
const Item = require('./../models/item.js');
// const Group = require('./../models/groups.js');

// index route
router.get('/items', function(req, res) {
  Item.find(function(err, items) {
    res.json(items)
  })
})

// show/view record
router.get('items/:id', function(req, res) {
  Item.findById(req.params.id, function(err, item) {
    if (err) {
      res.status(500).send('ERROR! Item not found.')
    } else {
      res.json(item)
    }
  })
})

// create a new record
router.post('/items', function(req, res) {
  const item = new Item(req.body)

  item.save(function(err) {
    if (err) {
      res.json(err)
    } else {
      res.status(201)
      res.json('Item created successfully!')
    }
  })
})

// update/modify goes to put/patch
router.put('/items/:id', function(req, res) {
  Item.findByIdAndUpdate(req.params.id, {
    name: req.body.name,
    groupId: ObjectId(req.body.groupId),
    completed: req.body.completed,
  }, { new: true }, function (err, item) {
    if (err) {
      res.json(err)
    } else {
      res.json(item)
    }
  })
})

// destroy/delete
router.delete('/items/:id', function(req, res) {
  Item.findByIdAndUpdate(req.params.id, (err, item) => {
    res.json(item)
  })
})

// return all items from a group
router.get('/groups/:id/items', function(req, res) {
  Item.find({ groupId: req.params.id }, function (err, items) {
    res.json(items)
  })
})

// search by name
router.get('/items/search/:search', function(req, res) {
  Item.findByName(req.params.search, function(err, items) {
    res.json(items)
    console.log(req.query)
  })
})

// make the object available to other
module.exports = router
