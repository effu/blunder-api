'use strict';
const express = require('express')
const router = express.Router()
const passport = require('passport')


const isLoggedIn = function (req, res, next) {
  if (req.isAuthenticated()) {
    return next()
  } else {
    res.redirect('/login')
  }
}

// pass the islogged in function into the /profile route
router.get('/profile', isLoggedIn, function(req, res) {
  res.render('profile.hbs', {
    user : req.user // get the user out of session and pass to template
  });
});

// router.get('/profile', function(req, res) {
//   res.render('profile')
// })

router.get('/login', function (req, res) {
  // render the function
  res.render('login')
})

router.get('/logout', function(req, res) {
  req.logout()
  res.redirect('/login')
})

// router.get('/auth/google', function (req, res) {
//   res.send('Google Login')
// })

router.get('/auth/google',
  passport.authenticate('google', {
    scope: ['profile', 'email']
  })
)

router.get('/auth/google/callback',
  passport.authenticate('google', {
    successRedirect: '/profile',
    failureReddirect: '/login'
  })
)


module.exports = router
