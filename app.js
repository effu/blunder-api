'use strict'
// import express
const express = require('express');
// creates a new express application
const app = express();
// import body-parser as body-parser
const mongoose = require('mongoose');

// passport
const passport = require('passport');
const BasicStrategy = require('passport-http').BasicStrategy
const handlebars = require('express-handlebars')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const session = require('express-session')

const items = require('./routes/items')
const auth = require('./routes/auth')
const groups = require('./routes/groups')
const users = require('./routes/users')
const User = require('./models/user')

const passportSetup = require('./config/passport')

passportSetup(passport)
// define routes
// method (path, handler)

// connect to your local DB
// mongod
mongoose.connect('mongodb://localhost/blunder')
// tell express we want to use .hbs files for handlebars
app.engine('.hbs', handlebars({
  defaultLayout: 'single',
  extname: '.hbs'
}))
app.set('view engine', '.hbs')

app.get('/v/:name', function (req, res) {
  res.render(req.params.name + '.hbs', {name: 'Frank',
    list:['js', 'node', 'mongo', 'react', 'redux' ] })
})

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

app.use(cookieParser())

app.use(session({secret: 'mysupersecretsession'}))
app.use(passport.initialize())
app.use(passport.session())
app.use(auth)

// app.post('/', function(req, res) {
//   res.json(req.body);
// });

app.get('/', function(req, res) {
  // grab response and send it
  // res.send('Hello world');
  // grab response and send as json
  //res.send('Hello world');
  res.render('login')
});


app.use(passport.initialize())
//app.all('*', passport.authenticate('basic', { session: false }))

app.use('/', auth)
app.use('/', items)
app.use('/', groups)
app.use('/', users)


// listen (port, handler)
app.listen(3000, function() {
  console.log('Blunderlist API listening on port 3000!');
});
