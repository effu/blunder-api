'use strict';


const mongoose = require('mongoose')
const Schema = mongoose.Schema

const itemSchema = new Schema({
  name: {
    type: String,
    minlength: [3, 'Name not long enough'],
    maxlength: [30, 'Name is too long'],
    required: [true, 'Name is required'],
  },
  groupId: {
    type: Schema.Types.ObjectId,
    red: 'Group',
    required: [true, 'An item needs a group. Create a group first!']
  },
  completed: {
    type: Boolean,
    default: false
  }

})

// assign a function to the "statics" object of our itemSchema
itemSchema.statics.findByName = function(name, cb) {
  return this.find({ name: new RegExp(name, 'i') }, cb);
};


module.exports = mongoose.model('Item', itemSchema);
