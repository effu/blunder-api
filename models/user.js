'use strict';
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
  firstName: {
    type: String,
    min: [3, 'Your first name is not long enough'],
    max: [30, 'Name is too long'],
    required: [true, 'First name is required'],
    default: null
  },
  lastName: {
    type: String,
    min: [3, 'Your last name not long enough'],
    max: [30, 'Name is too long'],
    required: [true, 'Last name is required'],
    default: null
  },
  username: {
    type: String,
    min: [6, 'Your username is not long enough'],
    max: [30, 'Name is too long'],
    required: [true, 'Username is required'],
    validate: [
      function(username, callback) {
        User.findOne({ username: username }, function (err, user) {
          if (err) { console.log(err) }
          if (user) {
            callback(false)
          } else {
            callback(true)
          }
        })
      },
      'Username already exist'
    ]
  },
  password: {
    type: String,
    minlegth: [6, 'Your password is not long enough'],
    required: [true, 'Password is required'],
  },
  dateCreated: {
    type: Date,
    default: new Date()
  },
  dateUpdated: {
    type: Date,
    default: new Date()
  },
  dateDeleted: {
    type: Date,
    default: null
  },
  google: {
    id: '',
    token: '',
    name: '',
    email: ''
  }
})



// assign a function to the "statics" object of our userSchema
userSchema.statics.findByUsername = function(name, cb) {
  return this.find({ name: new RegExp(username, 'i') }, cb);
};

userSchema.options.toJSON = {
  getters: true,
  setters: true,
  transform: function (doc, ret, options) {
    delete ret._id
    delete ret.__v
    return ret
  }
}


const User = mongoose.model('User', userSchema);

module.exports = User;
