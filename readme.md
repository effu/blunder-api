# API Building with Node.js and Express

## Node.js

Node.js® is a JavaScript runtime built on [Chrome's V8 JavaScript engine](https://developers.google.com/v8/). Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient. Node.js' package ecosystem, [npm](https://www.npmjs.com/), is the largest ecosystem of open source libraries in the world. [https://nodejs.org/en/](https://nodejs.org/en/)

## Express

Fast, unopinionated, minimalist web framework for [Node.js](https://nodejs.org/en/). Express is a minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications. **Building ****APIs - **With a myriad of HTTP utility methods and middleware at your disposal, creating a robust API is quick and easy. [https://expressjs.com/](https://expressjs.com/)** **

### Dixie State University - Code School

##### Week 4 - Day 1 - June 19, 2017

Instructor is** Kaden Wilkinson **

* He did the first Code School here at Dixie.

* Worked at busy busy for 3½ years.

* Now works at Vivint smart home

* Works on iOS and Android

    * Swift in xCode for iOS

        * Uses Swift

        * Little experience with objective C

    * Kotlin for Android

        * [https://developer.android.com/kotlin/index.html](https://developer.android.com/kotlin/index.html)

        * IntelliJ develops the Kotlin IDE

            * Fantastic tooling support

* Enjoys downhill mountain biking and BMX racing

* Played soccer in high school and was goalkeeper

* Served LDS Mission in Peru, speaks Spanish

#### Code School Sponsor

The sponsor this week is iGlobal Stores - Cross-Border eCommerce Technology - Add international cart software to any website [http://www.iglobalstores.com/](http://www.iglobalstores.com/)

## How browsers see the web

Browsers breakdown the URL you type to locate the correct server and resource.

### Parts of a URL

[http://www.webcarpenter.com:80/blog/120-How-to-make-a-Star-Trails-image-with-Photoshop?id=120#main](http://webcarpenter.com/blog/120-How-to-make-a-Star-Trails-image-with-Photoshop)

* Protocol / method

    * http://

* Domain Name

    * www.webcarpenter.com

* Port

    * :80

* Directory Path

    * /blog/

* Filename

    * [120-How-to-make-a-Star-Trails-image-with-Photoshop](http://webcarpenter.com/blog/120-How-to-make-a-Star-Trails-image-with-Photoshop)

* HTML Anchor

    * #main

* Parameters

    * ?id=120

### How a URL works

* Type URL into browser

* Goes to DNS server

    * DNS server returns an IP address

* Makes a request to the correct IP

* Server responds with

    * Code

    * Data

    * Receives the path and returns the data requested

* HTTP Headers

    * Key value pairs with responses

## What is CRUD?

CreateReadUpdateDestroy

## Status Codes

200 Success

201 Successfully created

204 No content

301 Redirect

404 Not Found

500 Internal Server error

## Applications that can send HTTP requests GET POST PUT DELETE

### Postman is free

[https://www.getpostman.com/apps](https://www.getpostman.com/apps)

### Paw

### Sample Paw Request

GET /groups HTTP/1.1

Accept: application/json

Host: localhost:3000

Connection: close

User-Agent: Paw/3.1.1 (Mac…)

### Using curl to test responses

Use the following format 

* curl -X METHOD URL

* Example

    * curl -X DELETE [http://localhost:3000/items/3](http://localhost:3000/items/3)

* Response

    * {"Items delete":{"id":"3"}}

## REST API (RESTful)

RESTful REpresentational State Transfer

A standard for communicating with different resources. 

API = Application Programming Interface

Retrieves the information from a server using a format that the other server can understand.

GET 		Show

POST 		Create

PATCH		Update

DELETE	Destroy

**Type		Method	Route		Code**

index		GET 		/items		200	

show		GET		/items/:id	200

create		POST 		/items		201

update		PUT/PATCH	/items/:id	204

destroy 	DELETE	/items/:id	204

### GraphQL

Developed by Facebook

## Setting Up Nodejs

1. Make a directory

    1. mkdir blunder-api

2. Run npm init

    2. Package name: blunder-api

    3. Version: 1.0.0

    4. Description: blunder

    5. Entry point: app.js

    6. Enter through the rest.

3. Add the Express package

    7. yarn add express

    8. yarn install

4. Create an app.js file your directory

5. Run this command to start your server

    9. node app.js

6. Make a folder for the routes

    10. mkdir route

### Sample app.js file

```js

// import express

const express = require('express');

// creates a new express application

const app = express();

// define routes

// method (path, handler)

app.get('/', function(req, res) {

  // grab response and send it

  res.send('Hello world');

});

// listen (port, handler)

app.listen(3000, function() {

  console.log('Blunderlist API listening on port 3000!');

});

```

## Universally unique identifier (UUID) 

A universally unique identifier (UUID) is a 128-bit number used to identify information in computer systems. The term globally unique identifier (GUID) is also used.

When generated according to the standard methods, UUIDs are for practical purposes unique, without depending for their uniqueness on a central registration authority or coordination between the parties generating them, unlike most other [numbering schemes](https://en.wikipedia.org/wiki/Numbering_scheme). While the probability that a UUID will be duplicated is not zero, it is so close to zero as to be negligible.

Thus, anyone can create a UUID and use it to identify something with near certainty that the identifier does not duplicate one that has already been, or will be, created to identify something else. Information labeled with UUIDs by independent parties can therefore be later combined into a single database, or transmitted on the same channel, without needing to resolve conflicts between identifiers.

## Database we are using with Node

NoSQL databases

MongoDB

[https://www.mongodb.com/](https://www.mongodb.com/)

Running the daemon to server the MongoDB mongod

To install MongoDB use the following command. Mongoose is the tool we are using.

```js

yarn add mongoose

```

### Models for Database Schema

Make a new directory called models

Add two new files for each resource

* items.js

* groups.js

### Creating a schema for Mongoose

Create a directory called **models/** in your server root.

```js

// models/items.js

const mongoose = require('mongoose')

const Schema = mongoose.Schema

const group = new Schema({

  id: UUID,

  name: {

    type: String,

    min: [3, 'Name not long enough'],

    required: [true, 'Name is required'],

  }

  groupId: Schema.Types.ObjectId,

  completed: Boolean,

})

module.exports = group

```

[http://mongoosejs.com/docs/guide.html](http://mongoosejs.com/docs/guide.html)

### Running the MongoDB tests

* Open a terminal and navigate to the root of the project

* execute npm install to install the necessary dependencies

* start a mongodb instance on port 27017 if one isn't running already. 

    * **mongod --dbpath <path to store data> --port 27017**

* execute npm test to run the tests (we're using [mocha](http://mochajs.org/))

    * or to execute a single test 

        * **npm test -- -g 'some regexp that matches the test description'**

    * any mocha flags can be specified with -- <mocha flags here>

    * For example, you can use 

        * **npm test -- -R spec** 

    * to use the spec reporter, rather than the dot reporter (by default, the test output looks like a bunch of dots)

## Day 2 - iGlobal Stores

### Kyle Erdley is presenting

Clint Reid is the CEO

He part of the first Code School. He was in construction and got a degree from BYU. He had some electrical engineering experience and wanted to learn some code.

He taught himself Ruby on Rails.

Had an opportunity to go to Code School and loved it.

He met some industry people

Websites for quilters with Dan Purcell and Velocity Webworks

Velocity Webworks got acquired by iGlobal stores.

He now works with iGlobal

### iGlobal Stores

He has the logic in place to calculate duties and taxes on the fly and determine if products are allowed to be sold or shipped in that area.

Technologies they use

* Google Vision API in Java

    * Get data from just looking at the image

    * Description, brand, price, etc…

* Java backend

* Javascript

* Verse framework

* SaaS - software as a service

* Polymer

    * Examples

        * Dropdowns with more enhanced functionality

        * Instead of div they have a smart div

Can you solve problems well?

Do you have the passion to follow through?

## Parts of a request

Method

Headers

Resource code

Body

URL / path

Query String / Form URL encoded

## Middleware

Middleware is software that acts as a bridge between an operating system or database and applications, especially on a network.

A chain of software layers that is between the server and client.

### Authentication with Middleware

An authentication layer would be ideal for most applications.

### Express Middleware

Parse incoming request bodies in a middleware before your handlers, available under the req.bodyproperty. [https://www.npmjs.com/package/body-parser](https://www.npmjs.com/package/body-parser)

```

yarn add body-parser

```

This module provides the following parsers:

* [JSON body parser](https://www.npmjs.com/package/body-parser#bodyparserjsonoptions)

* [Raw body parser](https://www.npmjs.com/package/body-parser#bodyparserrawoptions)

* [Text body parser](https://www.npmjs.com/package/body-parser#bodyparsertextoptions)

* [URL-encoded form body parser](https://www.npmjs.com/package/body-parser#bodyparserurlencodedoptions)

